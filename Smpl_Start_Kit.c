/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright (c) Nuvoton Technology Corp. All rights reserved.                                             */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "NUC1xx.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvGPIO.h"
#include "LCD_Driver.h"
#include "Seven_Segment.h"
#include "ScanKey.h"
#include "EEPROM_24LC64.h"
void delay_loop(void)
 {
 uint32_t i,j;
	for(i=0;i<3;i++)	
	{
		for(j=0;j<1000;j++);
    }
 
 }
 void counter(void){
	uint32_t i;
	for(i=0;i<5000;i++)	
	{
	}	
 }

/*----------------------------------------------------------------------------
  Interrupt subroutine
  ----------------------------------------------------------------------------*/
static unsigned char count=0;
static unsigned char loop=12;
void TMR0_IRQHandler(void) // Timer0 interrupt subroutine 
{ 
    unsigned char i=0;
 	TIMER0->TISR.TIF =1;
	count++;
	if(count==5)
	{
	   	DrvGPIO_ClrBit(E_GPC,loop);
	   	loop++;
	   	count=0;
	   	if(loop==17)
	   	{
	   		for(i=12;i<16;i++)
		   	{
	   			DrvGPIO_SetBit(E_GPC,i);	   
	   		}
			loop=12;
	   }
	}
}

void Timer_initial(void)
{
	/* Step 1. Enable and Select Timer clock source */          
	SYSCLK->CLKSEL1.TMR0_S = 0;	//Select 12Mhz for Timer0 clock source 
    SYSCLK->APBCLK.TMR0_EN =1;	//Enable Timer0 clock source

	/* Step 2. Select Operation mode */	
	TIMER0->TCSR.MODE=1;		//Select periodic mode for operation mode

	/* Step 3. Select Time out period = (Period of timer clock input) * (8-bit Prescale + 1) * (24-bit TCMP)*/
	TIMER0->TCSR.PRESCALE=0;	// Set Prescale [0~255]
	TIMER0->TCMPR  = 1000000;		// Set TICR(TCMP) [0~16777215]
								// (1/22118400)*(0+1)*(2765)= 125.01usec or 7999.42Hz

	/* Step 4. Enable interrupt */
	TIMER0->TCSR.IE = 1;
	TIMER0->TISR.TIF = 1;		//Write 1 to clear for safty		
	NVIC_EnableIRQ(TMR0_IRQn);	//Enable Timer0 Interrupt

	/* Step 5. Enable Timer module */
	TIMER0->TCSR.CRST = 1;		//Reset up counter
	TIMER0->TCSR.CEN = 1;		//Enable Timer0

  	TIMER0->TCSR.TDR_EN=1;		// Enable TDR function
}
/****FUNCTION****/
static char keybuff[4];

void initEEPROM(){
	DrvGPIO_InitFunction(E_FUNC_I2C1);
	if(Read_24LC64(0) != 255){//first use
		Write_24LC64(0,255);//for flag
		Write_24LC64(1,0);//for key counting purpose
	}
}

int getKeyN(int key){
	int count = Read_24LC64(1);
	if(key>=count){
		return 0;
		//return NULL;
	}else{
		keybuff[0] = Read_24LC64(2+(4*key));
		keybuff[1] = Read_24LC64(3+(4*key));
		keybuff[2] = Read_24LC64(4+(4*key));
		keybuff[3] = Read_24LC64(5+(4*key));
		return 1;
	}
}

void saveKey(char* key){
	int reg = Read_24LC64(1);
	Write_24LC64(2+(4*reg),key[0]);
	Write_24LC64(3+(4*reg),key[1]);
	Write_24LC64(4+(4*reg),key[2]);
	Write_24LC64(5+(4*reg),key[3]);
	Write_24LC64(1,reg+1);
}

void resetKey(){
	Write_24LC64(0,0);
	Write_24LC64(1,0);
}

int maxPage(){
	int count = Read_24LC64(1);
	return ((count/6) + 1);
}

int printPage(int page){
	int count = Read_24LC64(1);
	int i = 0;
	int row = 0;
	int reg = (page - 1)*6;
	if(reg < count){
		char buffer[16];
		//Init
		sprintf(buffer,"Halaman %d", page);
		print_lcd(row,buffer);
		row++;

		//Getting the keys
		
		while(getKeyN(reg) && reg<(page * 6)){
			for(i=0;i<4;i++){
				buffer[i] = keybuff[i];	
			}
			buffer[i] = ' ';
			reg++;

			if(getKeyN(reg)){
				for(i=5;i<9;i++){
					buffer[i] = keybuff[i-5];	
				}
				buffer[i] = '\0';				
			}else{
				buffer[i] = '\0';
			}
			print_lcd(row,buffer);
			row++;
			reg++;
		}
		
		return 1; 
	}else{
		clr_all_pannal();
		return 0;
	}
	
}

int main(void)
{
	int i=0;
	int input;
	int num = 0;
	int ready = 1;
	int timer = 0;
	int push = 0;
	int prev = 0;
	int num_delay;
	int change = 0;
	int page=1;
	int kode[4] = {16,16,16,16};
	int password[18] = {0,1,0,2,3,4,5,6,7,8,9,0,10,11,12,13,14,15};
	char hasil[4];	
		
	/* Unlock the protected registers */	
	UNLOCKREG();
   	/* Enable the 12MHz oscillator oscillation */
	DrvSYS_SetOscCtrl(E_SYS_XTL12M, 1);
 
     /* Waiting for 12M Xtal stalble */
    SysTimerDelay(5000);
 
	/* HCLK clock source. 0: external 12MHz; 4:internal 22MHz RC oscillator */
	DrvSYS_SelectHCLKSource(0);		
    /*lock the protected registers */
	LOCKREG();				

	DrvSYS_SetClockDivider(E_SYS_HCLK_DIV, 0); /* HCLK clock frequency = HCLK clock source / (HCLK_N + 1) */

    for(i=12;i<16;i++)
	{		
		DrvGPIO_Open(E_GPC, i, E_IO_OUTPUT);
    }

	Initial_pannel();  //call initial pannel function
	clr_all_pannal();
		  	  
	Timer_initial();
	
	OpenKeyPad();
	initEEPROM();

	printPage(page);

	while(1)
	{
		input = Scankey();
		if (input > 0 && ready){
			num = input;
			timer = 0;
			push = 1;
			ready = 0;

			if (prev != num){ prev = num; num_delay = 3;change = 1;}

			if (change && (kode[3]>15) && (num<7)){
				change = 0;
				num_delay = 3;
				kode[3]=kode[2];
				kode[2]=kode[1];
				kode[1]=kode[0];
				kode[0]=password[(num-1)*3];
			}
			
			if (num == 7 && !ready)
			{
					kode[0] = kode[1];
					kode[1] = kode[2];
					kode[2] = kode[3];
					kode[3] = 16;
			}
		}
			
		if(push && (num < 7)){
			push = 0;
			if (num == prev){
				if ((num == 1) || (num == 4)){
					if (num_delay < 1){
						num_delay++;
					} else
						num_delay = 0;
				} else
				{
					if (num_delay < 2){
						num_delay++;
					} else
						num_delay = 0;
				}
				//kode[0] = password[(prev-1)*3+num_delay];
			} 
			/*
			else 
			{
				num_delay = 0;
			} */
			prev = num;
			kode[0] = password[(num-1)*3+num_delay];
		} else if (push)
		{
			push = 0;
			if ((num == 8) && (num == prev) && (!change) && (kode[0]>15)){
					prev = 0;
					/*kode untuk reset di sini*/
					kode[0] = 16;
					kode[1] = 16;
					kode[2] = 16;
					kode[3] = 16; 
					resetKey();

					page = 1;
					printPage(page);
			}
			else if (num == 8){
					/*validasi kalo kode belum 4 angka*/
					if (kode[3] < 15){
						for (i=0;i<4;i++){
							if (kode[i] < 10){
								hasil[3-i] = kode[i] +48;
							} else
							{
								hasil[3-i] = kode[i] + 55;
							}
						}
						/*kode untuk simpan memori di sini*/
						//string hasil uda merupakan kode
						saveKey(hasil);
						clr_all_pannal();
						printPage(page);
						/*7segmen di reset ke awal*/
						kode[0] = 16;
						kode[1] = 16;
						kode[2] = 16;
						kode[3] = 16;
					}
					
					change = 0;
					prev = num;
				} else if (num == 9)
					{
						/*kode untuk simpan next page*/
						if(page<maxPage()){
							page++;
						}else{
							page = 1;
						}
						clr_all_pannal();
						printPage(page);
						prev = num;
					} else
						{
							prev = num;
						}
		}
				
		if (input == 0){
			ready = 1;
		}
		
		for (i=0; i<4; i++){
			close_seven_segment();
			show_seven_segment(i,kode[i]);
			//delay_loop();
			counter();
		}

		timer++;
		if (timer > 50){
			change = 1;
		}
	}	  		
}


